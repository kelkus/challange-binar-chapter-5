const { response } = require('express')
const express = require('express')
const router = express.Router()
const users = []

// route level middleware
router.use((req,res,next)=>{
    console.log('router level middleware')
    next()
})

router.get('/login', (req,res,) => {
    res.render('login')
})
router.get('/register', (req,res,) => {
    res.render('register')
})
router.post('/login', (req,res) => {
    const {email,password} = req.body
   
    users.push({email,password})
    console.log(users)
    res.redirect('/')
})


module.exports = router